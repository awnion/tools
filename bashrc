#!/usr/bin/env bash

echo "[LOG] source ~/.bashrc"

[[ -f "$HOME/.tools/exports" ]] && source "$HOME/.tools/exports"
[[ -f "$HOME/.tools/aliases" ]] && source "$HOME/.tools/aliases"

if [[ -n $PS1 ]]; then
	# interactive shell
	# SFTP fix error "message too long"
	if [[ ${TERM} != "dumb" ]]; then
		# Custom bash prompt
		[[ -r "$HOME/.tools/bash_prompt" ]] && source "$HOME/.tools/bash_prompt"
	fi
fi

if [[ -n "$(which brew)" ]]; then
	# brew bashcompletions
	if [[ -r "$(brew --prefix)/etc/bash_completion" ]]; then
		source "$(brew --prefix)/etc/bash_completion"
	fi

	# colorize logs and outputs
	if [[ -r "$(brew --prefix grc)/etc/grc.bashrc" ]]; then
		source "$(brew --prefix grc)/etc/grc.bashrc"
	fi
fi

# source local export aliases etc (if need)
[[ -f "$HOME/.aliases" ]] && source "$HOME/.aliases"
[[ -f "$HOME/.exports" ]] && source "$HOME/.exports"

# check for supervisur and enable bash completion if need
[[ -f "/usr/bin/supervisorctl" ]] && source "$HOME/.tools/supervisorctl_bash_completion.sh"

# apply @angular/cli completion
if [[ -n "$(which ng)" ]]; then
	eval "$(ng completion --bash)"
fi

# apply pipenv completion
if [[ -n "$(which pipenv)" ]]; then
	eval "$(pipenv --completion)"
fi
