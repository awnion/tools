#!/bin/bash

_supervisorctl()
{
    if [[ $COMP_CWORD -eq 1 ]]; then
        local cur=${COMP_WORDS[COMP_CWORD]}
        local main_params=$(supervisorctl help | tail -n +4)
        COMPREPLY=( $(compgen -W "$main_params" -- $cur) )
    fi

    if [[ $COMP_CWORD -eq 2 ]]; then
        local cur=${COMP_WORDS[COMP_CWORD]}
        local status=$(sudo supervisorctl status &2>/dev/null)
        if [[ $? == 0 ]]; then
            local params=$( echo "$status" | awk '{print $1}' | tr '\n' ' ' )
            COMPREPLY=( $(compgen -W "$params" -- $cur) )
        fi
    fi
}
complete -F _supervisorctl supervisorctl
