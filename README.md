# This is yet another profile and configs for Bash and Git

## Requirements
* Bash

* Full support Mac OS X 10.8.* default `Terminal`

* Also work on ubuntu and kubuntu default terminals

## Install

I'd recommend to install [solarized](http://ethanschoonover.com/solarized) theme for your terminal

Then execute this
```bash
cd ~
git clone git@bitbucket.org:awnion/tools.git .tools
cd .tools
make
```

For first time you have to use follow command:
```bash
. ~/.bashrc
```

Then you can use alias `refresh` for that

## Have fun!

![ScreenShot](https://bitbucket.org/awnion/tools/downloads/screenshot.png)