build:
	rm -f ~/.gitconfig ~/.bashrc ~/.bash_profile ~/.hyper.js ~/.zshrc
	rm -rf ~/bin
	ln -s ~/.tools/bashrc ~/.bash_profile
	ln -s ~/.tools/bashrc ~/.bashrc
	ln -s ~/.tools/zshrc ~/.zshrc
	ln -s ~/.tools/hyper.js ~/.hyper.js
	ln -s ~/.tools/gitconfig ~/.gitconfig
	ln -s ~/.tools/bin ~/bin
	if [ ! -f "${HOME}/.vimrc" ]; then ln -s ~/.tools/mini.vimrc ~/.vimrc; fi
